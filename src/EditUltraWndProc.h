#ifndef _H_EDITULTRA_WNDPROC_
#define _H_EDITULTRA_WNDPROC_

#include "framework.h"

int BeforeWndProc( MSG *p_msg );
int BeforeDispatchMessage( MSG *p_msg );
int AfterWndProc( MSG *p_msg );

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);

#endif
